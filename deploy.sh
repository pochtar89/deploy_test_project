rm -r public/images/*
cp -r assets/images/. public/images
echo y | composer install
echo y | php artisan migrate
php artisan cache:clear
php artisan route:clear
php artisan config:clear
php artisan view:clear
